from flask import Flask, jsonify, render_template, json, Response, request, send_file
from flask_cors import CORS, cross_origin
import collections
import os
import datetime
import time
import requests
import sys
from dotenv import load_dotenv
import psycopg2
import pymongo
import bson
import subprocess
from subprocess import call
import boto3
import logging

print("Running DCL DAO Catalyst Backup API Server",flush=True)
app = Flask(__name__)
CORS(app)

#Global params
load_dotenv(os.path.join(os.path.dirname(__file__),".env"))
postgres_user = os.environ.get("postgres_user")
postgres_pw = os.environ.get("postgres_pw")
postgres_host = os.environ.get("postgres_host")
postgres_port = os.environ.get("postgres_port")
bucket_name = os.environ.get("bucket_name")
aws_access_key = os.environ.get("aws_access_key")
aws_secret_key = os.environ.get("aws_secret_key")
content_dir = os.environ.get("content_directory")
mongo_user = os.environ.get("mongo_user")
mongo_pw = os.environ.get("mongo_pw")
mongo_host = os.environ.get("mongo_host")
mongo_collection = os.environ.get("mongo_collection")

#Mongo Connect
client = pymongo.MongoClient("mongodb+srv://"+mongo_user+":"+mongo_pw+"@" + mongo_host +"/" + mongo_collection)
db = client[mongo_collection]

#Postgres
sql_params = {
  'database': 'content',
  'user': postgres_user,
  'password': postgres_pw,
  'host': postgres_host,
  'port': postgres_port
}

#AWS S3 Connect
session = boto3.Session(
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
)
s3 = session.resource('s3')

s3_client = boto3.client(
    's3',
    region_name='us-east-1',
    aws_access_key_id=aws_access_key,
    aws_secret_access_key=aws_secret_key,
)

port= 4200
host='0.0.0.0'

def query(query):
    try:
        con = psycopg2.connect(**sql_params)
        cur = con.cursor()
        cur.execute(query)
        res = cur.fetchall()
    except psycopg2.InterfaceError as e:
        print('{} - connection will be reset'.format(e),flush=True)
        # Close old connection 
        if con:
            if cursor:
                cursor.close()
            con.close()
        con = None
        cursor = None
    
        # Reconnect 
        con = psycopg2.connect(**sql_params)
        cursor = con.cursor()
        cur.execute(query)
        res = cur.fetchall()
    except psycopg2.DatabaseError as e:
        print(f'Error {e}',flush=True)
        sys.exit(1)

    finally:
        if con:
            con.close()
        return res

@app.route('/create-backup',methods=['GET'])
def create_backup():
   # Specify the output file name
    output_file = 'content.dump'

    # Construct the pg_dump command
    pg_dump_cmd = [
        'pg_dump',
        f"--dbname=postgresql://{postgres_user}:{postgres_pw}@{postgres_host}:{postgres_port}/content",
        '--format=custom',
        f'--file=./backups/{output_file}',
    ]

    # Execute the pg_dump command
    # add failure logic
    try:
      subprocess.run(pg_dump_cmd)
    except:
      return "export of DB failed",500
    try:
      upload_sql_to_aws(output_file)
    except:
      return "upload to AWS failed",500
    
    # add cleanup so we don't clutter the volume
    return "Backup created.",200

def upload_sql_to_aws(filename):
    s3.meta.client.upload_file(Filename='./backups/' + filename, Bucket=bucket_name, Key= '/sql-backups/' + str(datetime.datetime.now().date()) + '-content.dump')
    return "uploaded!"

#Get the unique addresses within a timeframe and return the earliest per
@app.route('/distinct-users/<start>',methods=['GET'])
@app.route('/distinct-users/<start>/<end>',methods=['GET'])
def distinct_users(start,end=None,keep_in_house=False):
    stopwatch = time.time() * 1000
    if not start:
        return "Insufficient parameters provided - please provide the start timestamp in milliseconds",400
    if not end:
        end = stopwatch
    start_date = datetime.datetime.fromtimestamp(float(start)/1000.0)
    end_date = datetime.datetime.fromtimestamp(float(end)/1000.0)
    print("Getting distinct users for " + str(start_date) + " to " + str(end_date))
    res = query("""SELECT DISTINCT ON (entity_pointers) entity_pointers, entity_timestamp FROM public.deployments where entity_type = 'profile' and entity_timestamp >= '""" +str(start_date)+ """' and entity_timestamp <= '"""+str(end_date)+"""' ORDER BY entity_pointers, entity_timestamp;""")
    print("Received data.",flush=True)
    try:
        output = {"start":start_date,"end":end_date,"unique-users":[]}
        for r in res:
            output['unique-users'].append({
                "_id":r[0][0].lower(),
                "first-appearance":r[1]
            })
    except:
        output = "No users found."
    if keep_in_house:
        return output
    else:
        return jsonify(output),200

#Backfill User Number Table - loop around distinct-users for one day at a time; ranks them in mongo
@app.route('/backfill-user-numbers/<start>',methods=['GET'])
@app.route('/backfill-user-numbers/<start>/<end>',methods=['GET'])
def backfill_user_number(start,end=None,keep_in_house=False):
    stopwatch = time.time() * 1000
    if not end:
        end = stopwatch    
    #iterate by day
    for t in range(int(start),int(end),86400000):
        users = distinct_users(t,t+86400000,True)
        print("Received data for " + str(datetime.datetime.fromtimestamp(float(t)/1000.0)),flush=True)
        #for u in users['unique-users']:
        bulk_operations = [pymongo.InsertOne(document) for document in users['unique-users']]
        try:
            result = db.userNumber.bulk_write(bulk_operations, ordered=False)
            print('Documents inserted successfully:', result.inserted_count)
        except pymongo.errors.BulkWriteError as e:
            print('Bulk write error occurred.')
        except:
            print("Other error or no data found.")
    
    #tag em
    count = db.userNumber.count_documents({})
    print(count,flush=True)
    for i in range (0,count,100000):
        pipeline = [
            {
                '$sort': {
                    'first-appearance': 1
                }
            }, 
            {'$skip':i},
            {'$limit':100000},
            {
                '$group': {
                    '_id': None, 
                    'docs': {
                        '$push': '$$ROOT'
                    }
                }
            }, {
                '$unwind': {
                    'path': '$docs', 
                    'includeArrayIndex': 'counter'
                }
            }, 
            {
                '$replaceRoot': {
                    'newRoot': {
                        '$mergeObjects': [
                            '$docs', {
                                'counter': {
                                    '$add': [
                                        '$counter', 1+i
                                    ]
                                }
                            }
                        ]
                    }
                }
            }, {
                '$merge': 'userNumberOutput'
            }
        ]
        db.userNumber.aggregate(pipeline,allowDiskUse=True)
    return "All done backfilling.",200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port) 
