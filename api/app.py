from flask import Flask, jsonify, render_template, json, Response, request, send_file
from flask_cors import CORS, cross_origin
import collections
import os
import datetime
import time
import requests
import sys
from dotenv import load_dotenv
import psycopg2
import subprocess
import pymongo
import bson
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_redoc import Redoc

print("Running DCL DAO Catalyst API Server",flush=True)
app = Flask(__name__)
CORS(app)
app.config['REDOC'] = {'spec_route': '/redoc', 'title': 'DCL DAO Catalyst API Documentation'}
redoc = Redoc(app,'./query.yml')

#Global params
load_dotenv(os.path.join(os.path.dirname(__file__),".env"))
postgres_user = os.environ.get("postgres_user")
postgres_pw = os.environ.get("postgres_pw")
postgres_host = os.environ.get("postgres_host")
postgres_port = os.environ.get("postgres_port")
mongo_user = os.environ.get("mongo_user")
mongo_pw = os.environ.get("mongo_pw")
mongo_host = os.environ.get("mongo_host")
mongo_collection = os.environ.get("mongo_collection")

#Mongo Connect
client = pymongo.MongoClient("mongodb+srv://"+mongo_user+":"+mongo_pw+"@" + mongo_host +"/" + mongo_collection)
db = client[mongo_collection]

#Postgres
sql_params = {
  'database': 'content',
  'user': postgres_user,
  'password': postgres_pw,
  'host': postgres_host,
  'port': postgres_port
}

#rate limiting - set as you will

limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["10 per minute"],
    storage_uri="mongodb+srv://"+mongo_user+":"+mongo_pw+"@"+mongo_host +"/login",
)

port= 4200
host='0.0.0.0'

def query(query):
    try:
        con = psycopg2.connect(**sql_params)
        cur = con.cursor()
        cur.execute(query)
        res = cur.fetchall()
    except psycopg2.InterfaceError as e:
        print('{} - connection will be reset'.format(e),flush=True)
        # Close old connection 
        if con:
            if cursor:
                cursor.close()
            con.close()
        con = None
        cursor = None
    
        # Reconnect 
        con = psycopg2.connect(**sql_params)
        cursor = con.cursor()
        cur.execute(query)
        res = cur.fetchall()
    except psycopg2.DatabaseError as e:
        print(f'Error {e}',flush=True)
        sys.exit(1)

    finally:
        if con:
            con.close()
        return res

@app.route('/health')
def health():
    return 'dcl-postgres server is up'

#Get all deployments for a given profile
@app.route('/profile-history/<address>',methods=['GET'])
def profile_history(address):
    ts = time.time() * 1000
    if not address:
        return "No address provided - please provide an address in the route as /profile-history/<address>",400
    print("Getting profile history for " + str(address))
    res = query("""select entity_id, entity_metadata, entity_timestamp from public.deployments where entity_pointers = '{"""+ str(address).lower()+"""}' order by entity_timestamp desc""")
    print("Received data.",flush=True)
    output = {"address":address,"profile-history":[]}
    for r in res:
        output['profile-history'].append({'content_hash':r[0],'data':r[1],'timestamp':r[2]})
    return jsonify(output),200

#Get a specific profile deployment
@app.route('/historical-profile/<content_hash>',methods=['GET'])
def historical_profile(content_hash):
    ts = time.time() * 1000
    if not content_hash:
        return "No content_hash provided - please provide an hash in the route as /historical-profile/<content_hash>. Content hashes can be obtained from the /profile-history/<address> endpoint.",400
    print("Getting profile history for " + str(content_hash))
    res = query("""select entity_metadata, entity_timestamp, deployer_address from public.deployments where entity_id = '"""+ str(content_hash)+"""' order by entity_timestamp desc""")
    print("Received data.",flush=True)
    #entire metadata json stored in psql
    r = res[0]
    output = {"address":r[2],"historical-profile":r[0],'timestamp':r[1]}
    return jsonify(output),200

#Get the first instance of a user address
@app.route('/first-appearance/<address>',methods=['GET'])
def first_appearance(address):
    stopwatch = time.time() * 1000
    if not address:
        return "Insufficient parameters provided - please provide an Ethereum address.",400
    print("Getting player info for " + str(address))
    res = query("""select entity_id, entity_metadata, entity_timestamp from public.deployments where entity_pointers = '{"""+ str(address).lower()+"""}' order by entity_timestamp asc limit 1""")
    print("Received data.",flush=True)

    print(res,flush=True)
    try:
        output = {"address":address,"first-appearance":res[0][2]}
    except:
        output = "No scene found."

    return jsonify(output),200

#user number - the order in which an address first showed up to DCL
@app.route('/user-number/<address>',methods=['GET'])
def user_number(address):
    if not address:
        return "No address provided.",400
    try:
        user_number = db.userNumberOutput.find({'_id':str(address.lower())})[0]
        return jsonify({
            'address':address,
            'user-number':user_number['counter'],
            'first-appearance':user_number['first-appearance']
        }),200
    except:
        return "No user found.",200

#user number - the order in which an address first showed up to DCL
@app.route('/address-by-user-number/<number>',methods=['GET'])
def address_by_number(number):
    if not number:
        return "No user number provided.",400
    try:
        user_number = db.userNumberOutput.find({'counter':int(number)})[0]
        return jsonify({
            'address':user_number['_id'],
            'user-number':user_number['counter'],
            'first-appearance':user_number['first-appearance']
        }),200
    except:
        return "No user found.",200

#user number - the order in which an address first showed up to DCL
@app.route('/total-users',methods=['GET'])
@app.route('/total-users/<timestamp>',methods=['GET'])
def total_users(timestamp=None):
    if not timestamp:
        asOf = datetime.datetime.now()
    else:
        asOf = datetime.datetime.fromtimestamp(float(timestamp)/1000.0)
    
    try:
        user_number = db.userNumberOutput.count_documents({'first-appearance':{'$lte':asOf}})
        return jsonify({
            'total-users':user_number
        }),200
    except:
        return "No data found.",200     

#user number - the order in which an address first showed up to DCL
@app.route('/total-users-timeseries',methods=['GET'])
def total_users_timeseries():
    
    try:
        user_number = list(db.userNumberOutput.aggregate([
            {
                '$group': {
                '_id': {
                    '$dateToString': { 'format': "%Y-%m-%d", 'date': "$first-appearance" }
                    },
                'users': { '$min': "$counter" }
                }
            },
            {"$sort":{"users":-1}},
            {"$project":{"date":"$_id","users":1,"_id":0}}
            ]))
        return jsonify({
            'total-users':user_number
        }),200
    except:
        return "No data found.",200       

#Get all deployments for a given scene
@app.route('/scene-history/<x>/<y>',methods=['GET'])
def scene_history(x,y,timestamp=None):
    ts = time.time() * 1000
    if not x and not y:
        return "Scene coordinates not provided - please provide an x and y in the route as /scene-history/<x>/<y>",400
    print("Getting scene history for [" + str(x) + "," + str(y) +"]")
    res = query("""select entity_id, entity_metadata, entity_timestamp from public.deployments where entity_pointers = '{\""""+ str(x) + "," + str(y) +"""\"}' order by entity_timestamp desc""")
    print("Received data.",flush=True)
    output = {"coordinates":[int(x),int(y)],"scene-history":[]}
    for r in res:
        output['scene-history'].append({'content_hash':r[0],'data':r[1],'timestamp':r[2]})
    return jsonify(output),200

#Get a specific scene deployment
@app.route('/historical-scene/<content_hash>',methods=['GET'])
def historical_scene(content_hash):
    ts = time.time() * 1000
    if not content_hash:
        return "No content_hash provided - please provide an hash in the route as /historical-scene/<content_hash>. Content hashes can be obtained from the /scene-history/<x>/<y> endpoint.",400
    print("Getting scene history for " + str(content_hash))
    res = query("""select entity_metadata, entity_timestamp, entity_pointers from public.deployments where entity_id = '"""+ str(content_hash)+"""'  order by entity_timestamp desc""")
    print("Received data.",flush=True)
    #entire metadata json stored in psql
    r = res[0]
    output = {"coordinates":[int(r[2][0].split(",")[0]),int(r[2][0].split(",")[1])],"historical-scene":r[0],'timestamp':r[1]}
    return jsonify(output),200

#Get a specific scene deployment
@app.route('/scene-as-of/<x>/<y>/<timestamp>',methods=['GET'])
def scene_as_of(x,y,timestamp):
    stopwatch = time.time() * 1000
    if not x or not y or not timestamp:
        return "Insufficient parameters provided - please provide the x,y parcel coordinates and a timestamp in milliseconds",400
    ts = datetime.datetime.fromtimestamp(float(timestamp)/1000.0)
    print("Getting scene history for " + str(x) + "," + str(y) + " as of " + str(ts))
    res = query("""select entity_id, entity_metadata, entity_timestamp, deleter_deployment from public.deployments where entity_pointers = '{\""""+ str(x) + "," + str(y) +"""\"}' and entity_timestamp <= '"""+str(ts)+"""' order by entity_timestamp desc limit 1""")
    print("Received data.",flush=True)
    try:
        output = {"coordinates":[x,y],"CID":res[0][0],"historical-scene":res[0][1],'timestamp':res[0][2]}
        if not res[0][3]:
            output['is-active'] = True
        else:
            output['is-active'] = False
    except:
        output = "No scene found."

    return jsonify(output),200

#Get count of deployment by parcel
@app.route('/parcel-update-counts',methods=['GET'])
def parcel_update_count(x,y,timestamp=None):
    ts = time.time() * 1000
    res = query("""SELECT unnest(entity_pointers) AS coordinate, COUNT(*) AS occurrences, max(entity_timestamp) as mostRecent
                    FROM public.deployments where entity_type='scene'
                    GROUP BY coordinate order by occurrences desc;""")
    print("Received data.",flush=True)
    output = {"parcel-update-count":[{'parcel':r[0], 'last-update':r[2], 'count':r[1]} for r in res]}
    return jsonify(output),200

#Get count of deployments per day
@app.route('/deployments-per-day',methods=['GET'])
def deployments_per_day():
    res = query("""SELECT DATE(entity_timestamp) AS date, COUNT(id) AS count
                    FROM public.deployments
                    WHERE entity_type = 'scene'
                    GROUP BY DATE(entity_timestamp)
                    order by date asc;""")
    print("Received data.",flush=True)
    output = {"deployments-per-day":[{'_id':r[0], 'count':r[1]} for r in res]}
    return jsonify(output),200

#POSTMAN collection
@app.route('/postman',methods=['GET'])
def postman():
    return send_file('./dao_catalyst_data_api.postman_collection.json')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port) 
