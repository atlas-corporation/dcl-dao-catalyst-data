openapi: "3.0.0"
info:
  version: 1.0.0
  title: DAO Catalyst Data Query
  x-logo:
    url: https://atlascorp.mypinata.cloud/ipfs/QmWAg4DMZ9pAqCF2J6XK7zAZGrFxwUuRuWbkptp1UAtapN
    href: https://atlascorp.io
    backgroundColor: '#000000'
    altText: 'Atlas CORP'
  x-meta:
    title: Decentraland Catalyst Data APIs
    description: Open API for Decentraland Catalyst Data powered by Atlas CORP courtesy of the Decentraland DAO
    image: https://atlascorp.mypinata.cloud/ipfs/QmWAg4DMZ9pAqCF2J6XK7zAZGrFxwUuRuWbkptp1UAtapN
  license:
    name: MIT
servers:
  - url: https://dao-data.atlascorp.io/
paths:
  /profile-history/<address>:
    get:
      summary: /profile-history/<address>
      description: Get a list of profile updates for a given user through time.
      operationId: getProfileHistory
      tags:
        - profiles
      parameters:
        - name: address
          in: path
          description: The user's web3 wallet address
          required: true
          schema:
            type: string
            example: "0xE400A85a6169bd8BE439bB0DC9eac81f19f26843"
      responses:
        '200':
          description: An array of user profiles
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/profileHistory"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"  
  /historical-profile/<contentHash>:
    get:
      summary: /historical-profile/<contentHash>
      description: Get a specific version of a user profile given a content hash.
      operationId: getHistoricalProfile
      parameters:
        - name: contentHash
          in: path
          description: The content Hash associated with a file for download
          required: true
          schema:
            type: string
            example: "bafkreigebllb4w4kbih7rvnh227zng2wxhbga2ygloafwbbplfp4yjsgwa"      
      tags:
        - profiles
      responses:
        '200':
          description: An single user profile object
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/historicalProfile"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error" 
  /first-appearance/<address>:
    get:
      summary: /first-appearance/<address>
      description: Get the date a web3 wallet address first made a profile in Decentraland
      operationId: getFirstAppearance
      tags:
        - profiles
      parameters:
        - name: address
          in: path
          description: The user's web3 wallet address
          required: true
          schema:
            type: string
            example: "0xE400A85a6169bd8BE439bB0DC9eac81f19f26843"        
      responses:
        '200':
          description: An object with an address and the date time for first appearance
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/firstAppearance"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /scene-history/<x>/<y>:
    get:
      summary: /scene-history/<x>/<y>
      description: Get an array of scene commit data for the entire history of a given X,Y parcel in Decentraland
      operationId: getSceneHistory
      tags:
        - scenes
      parameters:
        - name: x
          in: path
          description: The X coordinate associated with land in DCL
          required: true
          schema:
            type: integer
            example: 36
        - name: y
          in: path
          description: The Y coordinate associated with land in DCL
          required: true
          schema:
            type: integer
            example: -6       
      responses:
        '200':
          description: An array of scene commits for a given parcel
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/sceneHistory"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error" 
  /scene-as-of/<x>/<y>/<timestamp>:
    get:
      summary: /scene-as-of/<x>/<y>/<timestamp>
      description: Get a scene commit data that was deployed as of a given timestamp in milliseconds
      operationId: getSceneAsOf
      tags:
        - scenes
      parameters:
        - name: x
          in: path
          description: The X coordinate associated with land in DCL
          required: true
          schema:
            type: integer
            example: 36
        - name: y
          in: path
          description: The Y coordinate associated with land in DCL
          required: true
          schema:
            type: integer
            example: -6       
        - name: timestamp
          in: path
          description: The timestamp in which to lookup the historical scene deployment
          required: true
          schema:
            type: integer
            example: 1684950811000           
      responses:
        '200':
          description: A historical scene commit for a given parcel
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/sceneAsOf"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /historical-scene/<contentHash>:
    get:
      summary: /historical-scene/<contentHash>
      description: Get a scene commit data that was deployed as of a given timestamp in milliseconds
      operationId: getHistoricalScene
      tags:
        - scenes
      parameters:
        - name: contentHash
          in: path
          description: The content Hash associated with a file for download
          required: true
          schema:
            type: string
            example: "bafkreigebllb4w4kbih7rvnh227zng2wxhbga2ygloafwbbplfp4yjsgwa"           
      responses:
        '200':
          description: A historical scene commit for a given parcel
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/historicalSceneObject"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"                
  /user-number/<address>:
    get:
      summary: /user-number/<address>
      description: Get a user number for a given web3 wallet address
      operationId: getUserNumber
      tags:
        - derived
      parameters:
        - name: address
          in: path
          description: The user's web3 wallet address
          required: true
          schema:
            type: string
            example: "0xE400A85a6169bd8BE439bB0DC9eac81f19f26843"                 
      responses:
        '200':
          description: A user number for a given web3 wallet address
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/userNumber"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"    
  /address-by-user-number/<userNumber>:
    get:
      summary: /address-by-user-number/<userNumber>
      description: Get a user number for a given web3 wallet address
      operationId: getAddressByUserNumber
      tags:
        - derived
      parameters:
        - name: userNumber
          in: path
          description: The user number to look up
          required: true
          schema:
            type: string
            example: 15               
      responses:
        '200':
          description: A user number for a given web3 wallet address
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/userNumber"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"    
  /total-users:
    get:
      summary: /total-users
      description: Get total number of users (who created a DCL profile)
      operationId: getTotalUsers
      tags:
        - derived              
      responses:
        '200':
          description: Total number of user profiles created in DCL
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/totalUsers"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"   
  /total-users/<timestamp>:
    get:
      summary: /total-users/<timestamp>
      description: Get total number of users (who created a DCL profile) as of a certain historical date
      operationId: getTotalUsersAsOf
      tags:
        - derived              
      responses:
        '200':
          description: Total number of user profiles created in DCL as of a historiacl date
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/totalUsers"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error" 
  /total-users-timeseries:
    get:
      summary: /total-users-timeseries
      description: Get total number of users (who created a DCL profile) as a daily  time series
      operationId: getTotalUserTimeSeries
      tags:
        - derived              
      responses:
        '200':
          description: Total number of user profiles created in DCL over time
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/totalUsersTimeSeries"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"   
  /parcel-update-count:
    get:
      summary: /parcel-update-count
      description: The number of times each individual parcel was updated, both as part of a larger scene or independently.
      operationId: parcelUpdateCount
      tags:
        - derived              
      responses:
        '200':
          description: The number of times each individual parcel was updated in DCL
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/parcelUpdateCount"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"     
  /deployments-per-day:
    get:
      summary: /deployments-per-day
      description: The number of scene updates to catalyst servers per day over time.
      operationId: deploymentsPerDay
      tags:
        - derived              
      responses:
        '200':
          description: A time series denoting the number of times scenes were updated each day.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/deploymentsPerDay"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"                                                                                                                                                                           
  /postman:
    get:
      summary: /postman
      description: Returns a file you can import into Postman to test and simulate the current list of API calls.
      operationId: getPostman    
      tags:
        - developerTools                      
      responses:
        '200':
          description: A JSON collection for import into Postman
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"                                                  
components: 
  schemas:
    color:
      type: object
      required:
        - a
        - b
        - g
        - r
      properties:
        a:
          type: number
          format: float
          description: "alpha channel"
          example: 1
        b:
          type: number
          format: float
          description: blue channel
          example: .1953125
        g:
          type: number
          format: float
          description: green channel
          example: .22265625
        r:
          type: number
          format: float
          description: red channel
          example: 0.37109375                              
    snapshots:
      type: object
      required:
        - body
        - face256
      properties:
        body:
          type: string
          description: hash of an image of the entire avatar
          example: "bafkreiblqxy4u53uvgsyy3iukycskeefwxvnyye4ts4lxlkky73ydd7s5y"
        face256:
          type: string
          description: hash of an image of the avatar's face 256 pixels square
          example: "bafkreifxtkwrgzclbjwdejw6efjloyipzkpddor45phgaorgwlbr7ekoq4"
    skin:
      type: object
      required:
        - color
      properties:
        color:
          type: object
          $ref: "#/components/schemas/color"  
    hair:
      type: object
      required:
        - color
      properties:
        color:
          type: object
          $ref: "#/components/schemas/color"  
    eyes:
      type: object
      required:
        - color
      properties:
        color:
          type: object
          $ref: "#/components/schemas/color"  
    emote:
      type: object
      required:
        - slot
        - urn
      properties:
        slot:
          type: integer
          description: Emote slot number
          example: 0
        urn:
          type: string
          description: the URN for the emote
          example: handsair
    avatar_data:
      type: object
      required:
        - bodyShape
        - emotes
        - eyes
        - hair
        - skin
        - snapshots
        - wearables
      properties:
        bodyShape:
          type: string
          description: "Users choice of body shape"  
          example: "urn:decentraland:off-chain:base-avatars:BaseMale"
        emotes:
          type: array
          items:
            $ref:  "#/components/schemas/emote"  
        eyes:
          type: object
          $ref:  "#/components/schemas/eyes"
        hair:
          type: object
          $ref:  "#/components/schemas/hair"
        skin:
          type: object
          $ref:  "#/components/schemas/skin"
        snapshots:
          type: object
          $ref:  "#/components/schemas/snapshots"
        wearables:
          type: array
          items:
            type: string
            description: "wearable URNs"
            example: "urn:decentraland:matic:collections-v2:0x1cf028020e8d77df0f4b934a308f67e6f11333ff:1"
    avatar:
      type: object
      required:
        - avatar
        - description
        - email
        - hasClaimedName
        - hasConnectedWeb3
        - interests
        - name
        - tutorialStep
        - unclaimedName
        - userId
        - version
      properties:
        avatar:
          type: object
          $ref: "#/components/schemas/avatar_data"  
        description:
          type: string
          description: "User-supplied profile description"
          example: "robots r ppl too"
        email:
          type: string
          description: "User-supplied email address"
          example: "howie@atlascorp.io"
        hasClaimedName:
          type: bool
          description: "Is a user using a purchased Name"
          example: true
        hasConnectedWeb3:
          type: bool
          description: "Is a user using a connected web3 wallet address"
          example: true    
        interests:
          type: array
          items:
            - string
          example: []      
        name:
          type: string
          description: "Username"
          example: "howieDoin"
        tutorialStep:
          type: integer
          description: "The step of the DCL tutorial the user is on"
          example: 0
        unclaimedName:
          type: string
          description: "The name the user intends to display, without numbers if added due to unowned name"
          example: "howieDoin"
        userId:
          type: string
          description: "The user web3 wallet address, or the generated guest ID used for the user"
          example: "0xe400a85a6169bd8be439bb0dc9eac81f19f26843"
        version:
          type: integer
          description: "The version of this user's profile"
          example: 45
    avatars:
      type: object
      required:
        - avatars
      properties:
        avatars:
          type: array
          items:
            $ref: "#/components/schemas/avatar"  
    profile_version:
      type: object
      required:
        - v
      properties:
        v:
          type: object
          $ref: "#/components/schemas/avatars"
    profile:
      type: object
      required:
        - content_hash
        - data
        - timestamp
      properties:
        content_hash:
          type: string
          description: "Hash of profile data as saved on Catalyst server"
          example: "bafkreigebllb4w4kbih7rvnh227zng2wxhbga2ygloafwbbplfp4yjsgwa"
        data:
          type: object
          $ref: "#/components/schemas/profile_version"
        timestamp:
          type: number
          format: float
          description: "Timestamp of last time data collected"  
    profileHistory:
      type: object
      required:
        - address
        - profile-history
      properties:
        address:
          type: string
          description: "User web3 wallet address"
          example: "0xE400A85a6169bd8BE439bB0DC9eac81f19f26843"
        profile-history:
          type: array
          items:
            $ref: "#/components/schemas/profile"        
    historicalProfile:
      type: object
      required:
        - address
        - historical-profile
      properties:
        address:
          type: string
          description: "User web3 wallet address"
          example: "0xE400A85a6169bd8BE439bB0DC9eac81f19f26843"
        historical-profile:
          type: object
          $ref: "#/components/schemas/profile_version"  
    firstAppearance:
      type: object
      required:
        - address
        - first-appearance
      properties:
        address:
          type: string
          description: "User web3 wallet address"
          example: "0xE400A85a6169bd8BE439bB0DC9eac81f19f26843"
        first-appearance:
          type: string
          description: ISO date time
          example: "Sat, 05 Jun 2021 13:25:24 GMT"
    communications:
      type: object
      required:
        - signalling
        - type
      properties:
        signalling:
          type: string
          example: "https://signalling-01.decentraland.org"
        type:
          type: string
          example: "webrtc"
    contact:
      type: object
      required:
        - email
        - name
      properties:
        email:
          type: string
          example: "howieDoin@atlascorp.io"
        name:
          type: string
          example: "howieDoin"
    display:
      type: object
      required:
        - favicon
        - title
      properties:
        favicon:
          type: string
          example: ""
        title:
          type: string
          example: "Atlas North"
    policy:
      type: object
      required:
        - blacklist
        - contentRating
        - fly
        - voiceEnabled
      properties:
        blacklist:
          type: array
          example: []
        contentRating:
          type: string
          example: "E"
        fly:
          type: bool
          example: true
        voiceEnabled:
          type: bool
          example: true
    scene:
      type: object
      required:
        - base
        - parcels
      properties:
        base:
          type: string
          example: "36,-6"
        parcels:
          type: array
          items:
            type: string
            example: "36,-6"
    spawnPoint:
      type: object
      required:
        - cameraTarget
        - default
        - name
        - position
      properties:
        cameraTarget:
          type: object
          $ref: "#/components/schemas/target"
        default:
          type: bool
          example: true
        name:
          type: string
          example: "spawn1"
        position:
          type: object
          $ref: "#/components/schemas/target"  
    target:
      type: object
      required:
        - x
        - y
        - z
      properties:
        x:
          type: string
          description: x coordinate
          example: 8
        y:
          type: string
          description: y coordinate
          example: 1
        z:
          type: string
          description: z coordinate
          example: 8   
    scene_data:
      type: object
      required:
        - communications
        - contact
        - display
        - main
        - owner
        - policy
        - scene
        - spawnPoints
        - tags
      properties:
        communications:
          type: object
          $ref: "#/components/schemas/communications"
        contact:
          type: object
          $ref: "#/components/schemas/contact" 
        display:
          type: object
          $ref: "#/components/schemas/display"
        main:
          type: string
          example: "bin/game.js"
        owner:
          type: string
          example: "howieDoin"
        policy:
          type: object
          $ref: "#/components/schemas/policy"
        spawnPoints:
          type: array
          items:
            $ref: "#/components/schemas/spawnPoint"
        tags:
          type: array
          items:
            type: string
            example: "atlas:*" 
    scene_version:
      type: object
      required:
        - v
      properties:
        v:
          type: object
          $ref: "#/components/schemas/scene_data"          
    historicalScene:
      type: object
      required:
        - content_hash
        - data
        - timestamp
      properties:
        content_hash:
          type: string
          description: "content hash of the scene data committed to the catalyst nodes"
        data:
          type: object
          $ref: "#/components/schemas/scene_version"     
        timestamp:
          type: string
          description: "ISO timestamp that the scene was deployed"                  
          example: "Thu, 20 Feb 2020 18:46:02 GMT"
    sceneHistory:
      type: object
      required:
        - coordinates
        - scene-history
      properties:
        coordinates:
          type: array
          description: "[X,Y] DCL coordinates"
          items:
            type: integer
          example: [36,-6]
        scene-history:
          type: array
          items:
            $ref: "#/components/schemas/historicalScene"   
    historicalSceneObject:
      type: object
      required:
        - coordinates
        - historical-scene
        - timestamp
      properties:
        coordinates:
          type: array
          description: "[X,Y] DCL coordinates"
          items:
            type: integer
          example: [36,-6]
        historical-scene:
          type: object
          $ref: "#/components/schemas/scene_version"     
        timestamp:
          type: string
          description: "ISO timestamp that the scene was deployed"                  
          example: "Thu, 20 Feb 2020 18:46:02 GMT"
    sceneHistory:
      type: object
      required:
        - coordinates
        - scene-history
      properties:
        coordinates:
          type: array
          description: "[X,Y] DCL coordinates"
          items:
            type: integer
          example: [36,-6]
        scene-history:
          type: array
          items:
            $ref: "#/components/schemas/historicalScene"                   
    sceneAsOf:
      type: object
      required:
        - CID
        - coordinates
        - historical-scene
        - is-active
        - timestamp
      properties:
        CID:
          type: string
          description: CID hash of the scene that was deployed as of the supplied timestamp
          example: "QmXbeYdzmPU6SxYQ2abP9DaLJ417e9ZGMnPML8VXnwFkhB"
        coordinates:
          type: array
          description: "[X,Y] DCL coordinates"
          items:
            type: integer
          example: [36,-6]
        historical-scene:
          type: object
          $ref: "#/components/schemas/historicalScene" 
        is-active:
          type: bool
          description: Is the scene data currently still deployed?
          example: true
        timestamp:
          type: string
          description: ISO timestamp that the returned scene commit was committed to the catalyst network
          example: "Thu, 20 Feb 2020 18:46:02 GMT"
    userNumber:
      type: object
      required:
        - address
        - first-appearance
        - user-number
      properties:
        address:
          type: string
          description: "User web3 wallet address"
          example: "0xE400A85a6169bd8BE439bB0DC9eac81f19f26843"
        first-appearance:
          type: string
          description: ISO date time
          example: "Sat, 05 Jun 2021 13:25:24 GMT"
        user-number:
          type: integer
          description: "The rank in which a user first made a profile in Decentraland"
          example: 196403 
    totalUsers:
      type: object
      required:
        - total-users
      properties:
        total-users:
          type: integer
          description: Total number of created user profiles in Decentraland
          example: 1412209 
    totalUsersByDate:
      type: object
      required:
        - users
        - date
      properties:
        users:
          type: integer
          description: Total number of created user profiles in Decentraland
          example: 1412209    
        date:
          type: string
          description: Date of observation
          example: "2023-06-01"
    totalUsersTimeSeries:
      type: object
      required:
        - total-users
      properties:
        total-users:
          type: array
          items:
            $ref: "#/components/schemas/totalUsersByDate" 
    parcelUpdate:
      type: object
      required:
        - parcel
        - last-update
        - count
      properties:
        parcel:
          type: string
          description: x,y coordinate
          example: "36,-6"    
        last-update:
          type: string
          description: Most recent update
          example: "2023-06-01"
        count:
          type: integer
          description: number of updates
          example: 100  
    parcelUpdateCount:
      type: object
      required: 
        - parcel-update-count
      properties:
        parcel-update-count:
          type: array
          items:
            $ref: "#/components/schemas/parcelUpdate" 
    sceneUpdatesPerDay:
      type: object
      required:
        - _id
        - count
      properties:
        _id:
          type: string
          description: Date
          example: "2023-06-01 00:00:00 GMT"
        count:
          type: integer
          description: number of updates
          example: 100              
    deploymentsPerDay:
      type: object
      required: 
        - deployments-per-day
      properties:
        deployments-per-day:
          type: array
          items:
            $ref: "#/components/schemas/sceneUpdatesPerDay" 
    Error:
      type: object
      required:
        - code
        - msg
      properties:
        code:
          type: integer
          format: int32
        msg:
          type: string
    Archive:
      type: object
      required:
        - date
        - download
        - filename
        - size-MB
      properties:
        date:
          type: string
          description: "The date timestamp of the daily archive file."
          example: "2022-12-31"
        download:
          type: string
          description: "The download link, expiring one hour from time of request."
          example: "https://dcl-dao-data-archive.s3.amazonaws.com/2022-12-31.tar?AWSAccessKeyId=..."
        filename:
          type: string
          description: "The name of the file."
          example: "2022-12-31.tar"
        size-MB:
          type: integer
          format: int32
          description: "The file size in Mb."
          example: 169.425860328674316          
    Archives:
      type: array
      items:
        $ref: "#/components/schemas/Archive" 

         