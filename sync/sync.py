import time
import datetime
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import boto3
import os
from dotenv import load_dotenv

print("Running DCL DAO Catalyst Content Sync Server",flush=True)

#Global params
load_dotenv(os.path.join(os.path.dirname(__file__),".env"))
aws_access_key = os.environ.get("aws_access_key")
aws_secret_key = os.environ.get("aws_secret_key")
bucket_name = os.environ.get("bucket_name")

class S3SyncHandler(FileSystemEventHandler):
	def __init__(self, source_directory, s3_bucket, s3_path):
		super().__init__()
		self.source_directory = source_directory
		self.s3_bucket = s3_bucket
		self.s3_path = s3_path
		self.s3_client = boto3.client(
			's3',
			region_name='us-east-1',
			aws_access_key_id=aws_access_key,
			aws_secret_access_key=aws_secret_key,
		)

	def sync_directory_to_s3(self, file_path):
		s3_key = f"{file_path}"
		try:
			self.s3_client.head_object(Bucket=self.s3_bucket, Key=s3_key)
		except self.s3_client.exceptions.ClientError as e:
			if e.response['Error']['Code'] == '404':
				self.s3_client.upload_file(file_path, self.s3_bucket, s3_key)
				timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
				print(f"New file uploaded: {file_path} ({timestamp})",flush=True)
			else:
				print(f"Error checking S3: {e}",flush=True)

	def on_modified(self, event):
		if not event.is_directory:
			file_path = event.src_path
			self.sync_directory_to_s3(file_path)

	def sync_directory_on_startup(self):
		for root, _, files in os.walk(self.source_directory):
			for file in files:
				file_path = os.path.join(root, file)
				self.sync_directory_to_s3(file_path)

def content_sync(source_directory, s3_bucket, s3_path):
	event_handler = S3SyncHandler(source_directory, s3_bucket, s3_path)
	observer = Observer()
	observer.schedule(event_handler, path=source_directory, recursive=True)
	observer.start()
	
	# Perform initial synchronization on startup
	event_handler.sync_directory_on_startup()

	try:
		while True:
			time.sleep(1)
	except KeyboardInterrupt:
		observer.stop()
		observer.join()

content_sync('/content', bucket_name, 'content')
